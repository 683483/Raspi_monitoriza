import sys
import time
import datetime

import gspread
import json

# from pydrive.drive import GoogleDrive
# from pydrive.auth import GoogleAuth
#
# # For using listdir()
# import os
#
# # Below code does the authentication
# # part of the code
# gauth = GoogleAuth()
#
# # Creates local webserver and auto
# # handles authentication.
# gauth.LocalWebserverAuth()
# drive = GoogleDrive(gauth)
#
# # replace the value of this variable
# # with the absolute path of the directory
# path = r"C:\Games\Battlefield"
#
# # iterating thought all the files/folder
# # of the desired directory
# for x in os.listdir(path):
#     f = drive.CreateFile({'title': x})
#     f.SetContentFile(os.path.join(path, x))
#     f.Upload()
#
#     # Due to a known bug in pydrive if we
#     # don't empty the variable used to
#     # upload the files to Google Drive the
#     # file stays open in memory and causes a
#     # memory leak, therefore preventing its
#     # deletion
#     f = None

# GDOCS_SPREADSHEET_NAME  = 'datos'
#
#
# FREQUENCY_SECONDS       = 30
#
# contador = 0
#
# def append_row(worksheet, data, counter):
#     contador = counter + len(data)
#     celda = 'A{0}'.format(contador)
#     print(celda)
#     worksheet.update(celda, data)
#     return contador
#
# def change_header(worksheet, header):
#     worksheet.update('A1', header)
#
# def login_open_sheet(spreadsheet):
#     try:
#         gc = gspread.service_account()
#         worksheet = gc.open(spreadsheet).sheet1
#         worksheet.update('A1', [[1, 2], [3, 4]])
#         return worksheet
#     except Exception as e:
#         print(e)
#         print('Unable to login and get spreadsheet')
#         sys.exit(1)
#
# worksheet = None
# counter = 0
# while True:
#
#     if worksheet is None:
#         worksheet = login_open_sheet(GDOCS_SPREADSHEET_NAME)
#         change_header(worksheet, [['fecha','counter']])
#         contador = contador + 1
#     counter = counter + 1
#     if counter > 2000:
#         counter = 0
#
#     try:
#         fecha = json.dumps(datetime.datetime.now(), default=str)
#         contador = append_row(worksheet, [[fecha,counter]], contador)
#     except Exception as e:
#         print('append error, logging in again')
#         worksheet = None
#         print (e)
#         time.sleep(4)
#         continue
#
#     print('Wrote a row to {0}'.format(GDOCS_SPREADSHEET_NAME))
#     time.sleep(4)