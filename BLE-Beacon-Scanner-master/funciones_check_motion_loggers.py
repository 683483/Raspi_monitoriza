import datetime
import smtplib, ssl
import gspread
import os
import csv
import sys
import numpy as np
from bluepy import btle
import time

if os.getenv('C', '1') == '0':
    ANSI_RED = ''
    ANSI_GREEN = ''
    ANSI_YELLOW = ''
    ANSI_CYAN = ''
    ANSI_WHITE = ''
    ANSI_OFF = ''
else:
    ANSI_CSI = "\033["
    ANSI_RED = ANSI_CSI + '31m'
    ANSI_GREEN = ANSI_CSI + '32m'
    ANSI_YELLOW = ANSI_CSI + '33m'
    ANSI_CYAN = ANSI_CSI + '36m'
    ANSI_WHITE = ANSI_CSI + '37m'
    ANSI_OFF = ANSI_CSI + '0m'



class motion_loggers_checker:

    TIEMPO_ENTRE_CHECKS = 300  # segundos

    CONFIG_SERVICE_UUID = b'\x00\x00\x00\x00\x00\x0f\x11\xe1\x9a\xb4\x00\x02\xa5\xd5\xc5\x1b'
    # CONFIG_SERVICE_UUID = '00000000-000f-11e1-9ab4-0002a5d5c51b'
    RX_CHAR_UUID = '00000002-000f-11e1-ac36-0002a5d5c51b'
    TX_CHAR_UUID = '00000003-0303-11e1-ac36-0002a5d5c51b'

    motion_loggers_upd = []             # Lista de handlers de los motion loggers actualizada
    columns = []                        # Lista con las macs que se estan monitorizando
    fail_devices = []                   # Lista con el numero de veces que los archivos de un tracker no han aumentado
    N_CHECKS_PARA_ERROR = 10            # Numero de comprobaciones que componen una ronda completa de comprobaciones
    contador_comprobaciones = 0         # Numero de comprobaciones de la ronda realizadas
    num_files_found = []                # Lista con el numero de archivos encontrado en cada tracker en cada escaneo
                                        # de una ronda
    filename = None                     # Nombre del csv donde se guardan los datos
    num_version = 0                     # Numero de version del documento .csv
    N_DISPOSITIVOS = 0                  # Numero de dispositivos monitorizados
    sensitivity = -128                  # Minimo rssi para establecer conexion
    limite_permitido = [0,  #anos       # Maxima diferencia temporal para conectar y actualizar la hora de un tracker
                       0,  #meses
                       0,  #dias
                       0,  #horas
                       2, #minutos
                       0]  #segundos
    row_counter = 2
    GDOCS_SPREADSHEET_NAME = 'datos'
    worksheet = None

    def __init__(self, devices, checks_distance, error_checks, limit):
        self.TIEMPO_ENTRE_CHECKS = checks_distance
        self.N_CHECKS_PARA_ERROR = error_checks
        self.limite_permitido[5] = limit % 60
        resto = limit // 60
        if resto > 0:
            self.limite_permitido[4] = resto % 60
            resto = resto // 60
            if resto > 0:
                self.limite_permitido[3] = resto % 24
                resto = resto // 24
                if resto > 0:
                    self.limite_permitido[2] = resto % 30
                    resto = resto // 30
                    if resto > 0:
                        self.limite_permitido[1] = resto % 12
                        resto = resto
                        if resto > 0:
                            self.limite_permitido[0] = resto
        self.update_motion_loggers(devices)
        self.columns = ['timestamp'] + [motion_logger.addr for motion_logger in self.motion_loggers_upd]

        date = datetime.datetime.now()
        self.filename = "files_check_{}_v{}.csv".format(date.strftime('%y%m%d%H%M%S'),self.num_version)
        with open(self.filename, 'w', newline='') as outfile:
            w = csv.writer(outfile)
            w.writerow(self.columns)

        self.num_files_found = np.zeros((self.N_CHECKS_PARA_ERROR, (self.N_DISPOSITIVOS + 1)))
        self.num_files_found = self.num_files_found.tolist()  # indica el numero de archivos encontrados en cada
        # observacion
        self.fail_devices = [0] * self.N_DISPOSITIVOS
        self.contador_comprobaciones = 1

        for i, tracker in enumerate(self.motion_loggers_upd):
            self.check_correct_hour(tracker,i)

    def rest(self):
        time.sleep(self.TIEMPO_ENTRE_CHECKS)

    def update_hour(self, dev):
        '''Busca el servicio y la caracteristica concreta en la que introducir el comando para cambiar la hora por la
        actual.'''
        services = sorted(dev.services, key=lambda s: s.hndStart)
        for service in services:
            if service.uuid.binVal == self.CONFIG_SERVICE_UUID:
                tx_char = service.getCharacteristics(forUUID=self.TX_CHAR_UUID)[0]

                date = datetime.datetime.now()

                command = str({'c': 'SD', 't': date.strftime('%H')})
                data = command.replace('\'', '\"').replace(' ', '').encode()[:-2]
                tx_char.write(data)

                command = str({date.strftime('%M%S%d%m%y')})
                data = command.replace('\'', '\"').replace(' ', '').encode()[2:]
                tx_char.write(data)

                rx_char = service.getCharacteristics(forUUID=self.RX_CHAR_UUID)[0]
                rx_value = rx_char.read()

                print(rx_value)
        pass

    def last_hope(self, tracker):
        '''Trata de conectarse al dispositivo para forzar el reseteo y salir de un posible atasco,
        es la última esperanza para sacar el dispositivo de un atasco.
        param:
            tracker     handler del motion logger para realizar la conexión
        '''
        print('Intento last_hope ')
        try:
            dev = btle.Peripheral(tracker)
            services = sorted(dev.services, key=lambda s: s.hndStart)
            for service in services:
                if service.uuid.binVal == self.CONFIG_SERVICE_UUID:
                    tx_char = service.getCharacteristics(forUUID=self.TX_CHAR_UUID)[0]

                    command = str({'c': 'SD', 'r': '1'})
                    data = command.replace('\'', '\"').replace(' ', '').encode()
                    tx_char.write(data)

                    rx_char = service.getCharacteristics(forUUID=self.RX_CHAR_UUID)[0]
                    rx_value = rx_char.read()

                    print(rx_value)

            dev.disconnect()
        except Exception as e:
            print('Conexion last_hope fallida: ')
            print(e)
        pass

    def diferencia_temporal(self, fecha1, fecha2):
        '''Comprueba si la diferencia entre dos fechas es superior a la indicada por el limite_permitido
        params:
                fecha1 primer sumando
                fecha2 segundo sumando
        return:
                True    si la diferencia entre fechas es menor que limite_permitido
                False   si la diferencia entre fechas es mayor que limite_permitido
        '''
        limite_segundos =   self.limite_permitido[0] * (60*60*24*30*12) + \
                            self.limite_permitido[1] * (60*60*24*30) + \
                            self.limite_permitido[2] * (60*60*24) + \
                            self.limite_permitido[3] * (60*60) + \
                            self.limite_permitido[4] * 60 + \
                            self.limite_permitido[5]
        diferencia =    (fecha1.year -2000   - fecha2[0])  * (60*60*24*30*12) + \
                        (fecha1.month        - fecha2[1])  * (60*60*24*30) + \
                        (fecha1.day          - fecha2[2])    * (60*60*24) + \
                        (fecha1.hour         - fecha2[3])   * (60*60) + \
                        (fecha1.minute       - fecha2[4]) * 60 + \
                        (fecha1.second       - fecha2[5])
        if limite_segundos > diferencia > 0:
            return True
        else:
            return False

    def formatear_advertising(self, tracker):
        '''Extrae del mensaje de advertising raw los tres parametros de interes, MAC, numero de archivos y fecha
        params:
            tracker     contiene toda la informacion del dispositivo
        return:
            Adv_Data    lista que contiene MAC, numero de archivos y fecha'''
        Adv_Data = [tracker.addr,
                    int.from_bytes(tracker.scanData[255][1:3], "little", signed=False),
                    [int.from_bytes(tracker.scanData[255][5:6], "little", signed=False),
                     int.from_bytes(tracker.scanData[255][6:7], "little", signed=False),
                     int.from_bytes(tracker.scanData[255][7:8], "little", signed=False),
                     int.from_bytes(tracker.scanData[255][8:9], "little", signed=False),
                     int.from_bytes(tracker.scanData[255][9:10], "little", signed=False),
                     int.from_bytes(tracker.scanData[255][10:11], "little", signed=False)],
                    int.from_bytes(tracker.scanData[255][3:5], "little", signed=False)]
        return Adv_Data

    def update_motion_loggers(self, devices):
        '''Comprueba si se los dispositivos de la lista devices se encuentran en columnas, si no estan los anade a la
        nueva lista de dispositivos actualizada motion_loggers_upd, si ya estaban se actualiza el handler para que la
        conexion tenga los datos actualizados.
        Params:
                devices             lista de handlers de los motion loggers detectados en el ultimo escaneo
        '''
        if len(self.columns) > 0:
            self.motion_loggers_upd = self.columns[1:]
        else:
            self.motion_loggers_upd = []
        print('----------------ROUND {}----------------------'.format(self.contador_comprobaciones))
        for d in devices:
            if not d.connectable or d.rssi < self.sensitivity:
                continue
            '''COMPROBAMOS SI EL DISPOSITIVO ES UN TRACKER Y AnADIMOS LOS NUEVOS'''
            if 9 in d.scanData.keys():
                if d.scanData[9] == b'TRACKER':
                    Adv_Data = self.formatear_advertising(d)
                    if d.addr not in self.columns:
                        print(
                            'NUEVO! Dispositivo {} tiene {} archivos, lleva {} medidas, fecha: {}-{}-{}, hora: {}:{}:{'
                            '}'.format(
                                Adv_Data[0], Adv_Data[1], Adv_Data[3], Adv_Data[2][2], Adv_Data[2][1], Adv_Data[2][0],
                                Adv_Data[2][3], Adv_Data[2][4], Adv_Data[2][5]))
                        self.motion_loggers_upd.append(d)
                    else:
                        print(
                            'Dispositivo {} tiene {} archivos, lleva {} medidas, fecha: {}-{}-{}, hora: {}:{}:{}'.format(
                                Adv_Data[0], Adv_Data[1], Adv_Data[3], Adv_Data[2][2], Adv_Data[2][1], Adv_Data[2][0],
                                Adv_Data[2][3], Adv_Data[2][4], Adv_Data[2][5]))
                        self.motion_loggers_upd[self.columns.index(d.addr)-1] = d
        self.N_DISPOSITIVOS = len(self.motion_loggers_upd)

    def check_correct_hour(self, tracker, i):
        '''Comprueba la hora del mensaje de advertising, si la diferencia con la hora actual es superior al
        limite se conecta y se cambia. Despues se guarda el numero de archivos almacenados.
        params:
            tracker     contiene toda la informacion del dispositivo
            i           indica el numero de la posicion del dispositivo en la lista
        '''
        if type(tracker) is not str:
            Adv_Data = self.formatear_advertising(tracker)
            '''Se comprueba si la hora esta actualizada, si no lo esta se actualiza'''
            if not self.diferencia_temporal(datetime.datetime.now(), Adv_Data[2]):
                print("    Connecting to", ANSI_WHITE + tracker.addr + ANSI_OFF + ":")
                try:
                    dev = btle.Peripheral(tracker)
                    self.update_hour(dev)
                    dev.disconnect()
                except Exception as e:
                    print('Conexion fallida: ')
                    print(e)
            self.num_files_found[self.contador_comprobaciones][i + 1] = Adv_Data[3] #Adv_Data[1]
            # self.measures_found[self.contador_comprobaciones][i + 1] = Adv_Data[3]
        else:
            self.num_files_found[self.contador_comprobaciones][i + 1] = 0
            # self.measures_found[self.contador_comprobaciones][i + 1] = 0

    def store_file_number_check_correct_hour(self):
        '''Hace una comprobacion de los dispositivos localizados en el ultimo escaneo y guarda los datos del numero
        de archivos almacenados. Si la hora indicada supera la diferencia maxima permitida se conecta para
        actualizarla.
        '''
        for i, tracker in enumerate(self.motion_loggers_upd):
            self.check_correct_hour(tracker, i)

            if self.contador_comprobaciones > 0:
                self.fail_devices[i] = self.comprueba_aumento_archivos(self.num_files_found[self.contador_comprobaciones][i+1], self.num_files_found[
                self.contador_comprobaciones-1][i+1], self.fail_devices[i], tracker)
        date = datetime.datetime.now()
        self.num_files_found[self.contador_comprobaciones][0] = date.strftime('%H%M%S%d%m%y')

    def comprueba_aumento_archivos(self, num_files_now, num_files_ant, num_fails, tracker):
        '''Comprueba si el numero de archivos del ultimo escaneo es mayor, igual o menor al del escaneo anterior
        Params:
                num_files_now   numero de archivos del ultimo escaneo
                num_files_ant   numero de archivos del escaneo anterior
                num_fails       numero de veces que el dispositivo no ha aumentado sus archivos
                tracker         handler del dispositivo para last_hope si fuera necesario
        Return:
                num_fails       numero de veces que el dispositivo no ha aumentado sus archivos actualizado
        '''
        if num_files_now == 0 or num_files_now == 2863311530:
            num_fails = num_fails + 1
        elif num_files_ant == num_files_now:
            num_fails = num_fails + 1
        else:
            num_fails = 0

        # if num_fails > 2*self.N_CHECKS_PARA_ERROR:
        #     self.last_hope(tracker)
        return num_fails

    def accion_final_comprobacion(self):
        '''Accion que se realiza al final de cada comprobacion. Si es el final de la ronda se guardan los datos en el
        .csv, se comprueba el numero de fails detectados y se manda correo si algun tracker supera el numero maximo
        permitido. Si no es la ultima comprobacion de la ronda se suma 1 al contador.
        '''
        if self.contador_comprobaciones < self.N_CHECKS_PARA_ERROR - 1:
            self.contador_comprobaciones = self.contador_comprobaciones +1
        else:
            '''Realizadas X comprobaciones se avisa al usuario con un email, si es necesario, de los dispositivos que
                                    estan fallando'''
            with open(self.filename, 'a', newline='') as f_object:
                writer_object = csv.writer(f_object)
                writer_object.writerows(self.num_files_found)
                f_object.close()
            self.gdrive_append_row()
            TEXT = None
            for i, n_fails in enumerate(self.fail_devices):
                if n_fails >= (self.N_CHECKS_PARA_ERROR - 1):
                    if TEXT == None:
                        TEXT = """El dispositivo MAC: {} esta fallando.""".format(self.columns[i+1])
                    else:
                        TEXT = TEXT + """\r\nEl dispositivo MAC: {} esta fallando.""".format(self.columns[i+1])
                    for tracker in self.motion_loggers_upd:
                        if type(tracker) is not str:
                            if tracker.addr == self.columns[i+1]:
                                self.last_hope(tracker)
            if TEXT:
                SUBJECT = "Monitorizacion ovejas"
                self.send_email(SUBJECT, TEXT)


            self.contador_comprobaciones = 0
            self.num_files_found = np.zeros((self.N_CHECKS_PARA_ERROR, (self.N_DISPOSITIVOS + 1)))
            self.num_files_found = self.num_files_found.tolist()

    def send_email(self, SUBJECT, TEXT):
        '''Envia un mail desde sender_email a receiver_email con el asunto y el texto indicados
        Params:
                SUBJECT     asunto del mensaje
                TEXT        cuerpo del mensaje
        '''
        port = 587  # 587 for raspi  #  465 For SSL
        smtp_server = "smtp.gmail.com"
        sender_email = "correodepruebas.agr@gmail.com"  # Enter your address
        receiver_email = "algaroche@unizar.es"  # Enter receiver address
        password = 'PassdeTest-21'  # input("Type your password and press enter: ")

        message = 'Subject: {}\r\n\n{}'.format(SUBJECT, TEXT)
        # https://www.google.com/settings/security/lesssecureapps ES NECESARIO ACTIVAR EL ACCESO DE APLICACIONES POCO SEGURAS
        context = ssl.create_default_context()

        # https://realpython.com/python-send-email/
        with smtplib.SMTP(smtp_server, port) as server:
            server.starttls()
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, message)

    def gdrive_login_open_sheet(self):
        '''Entra en la cuenta con el service_account.json y abre la spreadsheet para poder ir completandola'''
        try:
            gc = gspread.service_account()
            self.worksheet = gc.open(self.GDOCS_SPREADSHEET_NAME).sheet1
        except Exception as e:
            print(e)
            print('Unable to login and get spreadsheet')
            sys.exit(1)

    def gdrive_append_row(self):
        '''Anade los num_files_found a la spreadsheet abierta'''
        celda = 'A{}'.format(self.row_counter)
        print(celda)
        self.worksheet.update(celda, self.num_files_found)
        self.row_counter = self.row_counter + len(self.num_files_found)

    def gdrive_change_header(self):
        '''Se comprueba si se han detectado nuevos dispositivos que monitorizar, si es asi se incluyen en la lista de
        columnas y se modifican los headers tanto del .csv como de la spreadsheet'''
        for tracker in self.motion_loggers_upd:
            if type(tracker) is not str:
                Adv_Data = self.formatear_advertising(tracker)

                '''Se actualiza las columnas del csv incluyendo los dispositivos nuevos detectados'''
                if tracker.addr not in self.columns:
                    self.fail_devices.append(0)
                    self.columns.append(Adv_Data[0])
                    for lista in self.num_files_found:
                        lista.append(0)

                    '''Genero un nuevo csv para meter la nueva columna y copio todos los elementos del archivo anterior'''
                    filename_old = self.filename
                    self.num_version = self.num_version + 1
                    self.filename = filename_old[:-5] + '{}.csv'.format(self.num_version)
                    with open(filename_old, 'r') as inFile, open(self.filename, 'w', newline='') as outfile:
                        r = csv.reader(inFile)
                        w = csv.writer(outfile)

                        next(r, None)  # skip the first row from the reader, the old header
                        # write new header
                        w.writerow(self.columns)

                        # copy the rest
                        for row in r:
                            w.writerow(row)
                    os.remove(filename_old)

        if self.worksheet is None:
            self.gdrive_login_open_sheet()

        try:
            self.worksheet.update('A1',[self.columns])
        except Exception as e:
            print(e)


